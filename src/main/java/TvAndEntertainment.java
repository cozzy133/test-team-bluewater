public class TvAndEntertainment extends Product {

    private String screenSize;
    private boolean services;
    private String quality;
    private String model;

    public TvAndEntertainment() {
        super();
    }

    public TvAndEntertainment(String barcode, String description, String manufacture) {
        super(barcode, description, manufacture);
    }

    public TvAndEntertainment(String barcode, String description, String manufacture, String screenSize, boolean services, String quality, String model) {
        super(barcode, description, manufacture);
        setModel(model);
        setServices(services);
        setQuality(quality);
        setScreenSize(screenSize);
    }

    public String getScreenSize() {
        return screenSize;
    }

    public void setScreenSize(String screenSize) {
        if (screenSize.equals("")) {
            System.out.println("fill in all fields in setter");
            throw new IllegalArgumentException();
        } else {
            this.screenSize = screenSize;
        }
    }

    public boolean isServices() {
        return services;
    }

    public void setServices(boolean services) {
        if (!services) {
            System.out.println("fill in all fields in setter");
            throw new IllegalArgumentException();
        } else {
            this.services = services;
        }
    }

    public String getQuality() {
        return quality;
    }

    public void setQuality(String quality) {
        if (quality.equals("")) {
            System.out.println("fill in all fields in setter");
            throw new IllegalArgumentException();
        } else {
            this.quality = quality;
        }
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        if (model.equals("")) {
            System.out.println("fill in all fields in setter");
            throw new IllegalArgumentException();
        } else {
            this.model = model;
        }
    }

    @Override
    public String toString() {
        return super.toString() + "TvAndEntertainment{" +
                "screenSize='" + screenSize + '\'' +
                ", services=" + services +
                ", quality='" + quality + '\'' +
                ", model='" + model + '\'' +
                '}';
    }
}
