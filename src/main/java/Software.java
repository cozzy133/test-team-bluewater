public class Software extends Product {

    private String version;
    private int numberOfPeople;

    public Software() {
    }

    public Software(String barcode, String description, String manufacture) {
        super(barcode, description, manufacture);
    }

    public Software(String barcode, String description, String manufacture, String version, int numberOfPeople) {
        super(barcode, description, manufacture);
        setNumberOfPeople(numberOfPeople);
        setVersion(version);
    }

    public void setVersion(String version) {
        if (version.equals("")) {
            System.out.println("fill in all fields in setter");
            throw new IllegalArgumentException();
        } else {
            this.version = version;
        }
    }

    public String getVersion() {
        return version;
    }

    public int getNumberOfPeople() {
        return numberOfPeople;
    }

    public void setNumberOfPeople(int numberOfPeople) {
        if (numberOfPeople == 0) {
            System.out.println("fill in all fields in setter");
            throw new IllegalArgumentException();
        } else {
            this.numberOfPeople = numberOfPeople;
        }
    }

    @Override
    public String toString() {
        return super.toString() + "Software{" +
                "version='" + version + '\'' +
                ", numberOfPeople=" + numberOfPeople +
                '}';
    }
}
