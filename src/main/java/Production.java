/*
 * Author : Padraig O Cosgora
 * Email : G00311302@gmit.ie
 *
 * The TestingKitsProduction class extends Thread and creates an instance of a producer of test kits. These kits
 * are produced and passed to the HomeDeliveryOrderApplication object.
 *
 */

import java.util.ArrayList;

public class Production extends Thread {
    private final DeliveryOrderApplication orderBuf;
    ArrayList<Product> deliveryVan = new ArrayList<>();


    /**
     * This is the only constructor for the TestingKitsProduction class
     *
     * @param c   The HomeDeliveryOrderApplication object
     * @param p the number of products delivered
     */
    public Production(DeliveryOrderApplication c, ArrayList<Product> p) {
        orderBuf = c;
        deliveryVan = p;
    }

    /**
     * This method runs in a separate thread
     */
    public void run() {
        orderBuf.put(deliveryVan);
        System.out.println("Produced : " + deliveryVan);
    }

    public void setDeliveryVan(ArrayList<Product> deliveryVan) {
        this.deliveryVan = deliveryVan;
    }

    public ArrayList<Product> getDeliveryVan() {
        return deliveryVan;
    }
}
