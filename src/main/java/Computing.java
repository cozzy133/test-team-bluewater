public class Computing extends Product {

    /*
    Testing pipeline2
     */

    private String cpu;
    private String ram;
    private String screenSize;

    public Computing() {
    }

    public Computing(String barcode, String description, String manufacture) {
        super(barcode, description, manufacture);
    }

    public Computing(String barcode, String description, String manufacture, String cpu, String ram, String screenSize) {
        super(barcode, description, manufacture);
        setCpu(cpu);
        setRam(ram);
        setScreenSize(screenSize);
    }

    public String getCpu() {
        return cpu;
    }

    public void setCpu(String cpu) {
        if (cpu.equals("")) {
            System.out.println("fill in all fields in setter");
            throw new IllegalArgumentException();
        } else {
            this.cpu = cpu;
        }
    }

    public String getRam() {
        return ram;
    }

    public void setRam(String ram) {
        if (ram.equals("")) {
            System.out.println("fill in all fields in setter");
            throw new IllegalArgumentException();
        } else {
            this.ram = ram;
        }
    }

    public String getScreenSize() {
        return screenSize;
    }

    public void setScreenSize(String screenSize) {
        if (screenSize.equals("")) {
            System.out.println("fill in all fields in setter");
            throw new IllegalArgumentException("test");
        } else {
            this.screenSize = screenSize;
        }
    }

    @Override
    public String toString() {
        return super.toString() + "Computing{" +
                "cpu='" + cpu + '\'' +
                ", ram='" + ram + '\'' +
                ", screenSize='" + screenSize + '\'' +
                '}';
    }
}
