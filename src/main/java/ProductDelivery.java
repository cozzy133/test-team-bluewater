import java.util.ArrayList;

public class ProductDelivery {

    public ArrayList<Product> packTruck(int amount) {

        ArrayList<Product> deliveryVan = new ArrayList<>();

        Book book = new Book("010101", "Elon Reeve Musk FRS is an engineer, industrial designer, " +
                "technology entrepreneur, and philanthropist. He is a citizen of South Africa, the United States, " +
                "and Canada", "Virgin Books", "Elon Musk", "Penguin", "John Doe");

        Computing comp = new Computing("121212", "Fancy new Computer", "Nvidia",
                "Intel i9 9700k", "8 gb 2666 mhz", "25 inch");

        KitchenAppliances appliance = new KitchenAppliances("232323", "Fancy new Kettle",
                "M16", "Morphy Richards", 2, 2);

        Software sw = new Software("454545", "Fancy new software", "Oracle", "1.0.0",
                1);

        TvAndEntertainment tv = new TvAndEntertainment("676767", "Fancy new TV",
                "Samsung", "50inch", true, "8K", "HM121");

        for (int i = 0; i < amount; i++) {
            deliveryVan.add(book);
            deliveryVan.add(comp);
            deliveryVan.add(appliance);
            deliveryVan.add(sw);
            deliveryVan.add(tv);
        }

        return deliveryVan;
    }
}