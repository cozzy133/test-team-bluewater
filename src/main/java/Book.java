public class Book extends Product {

    private String author;
    private String publisher;
    private String editor;

    public Book() {
    }


    public Book(String barcode, String description, String manufacture, String author, String publisher, String editor) {
        super(barcode, description, manufacture);
        setAuthor(author);
        setEditor(editor);
        setPublisher(publisher);
    }

    public Book(String barcode, String description, String manufacture) {
        super(barcode, description, manufacture);
    }


    public void setAuthor(String author) {
        if (author.equals("")) {
            System.out.println("fill in all fields in setter");
            throw new IllegalArgumentException();
        } else {
            this.author = author;
        }
    }

    public String getAuthor() {
        return author;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        if (publisher.equals("")) {
            System.out.println("fill in all fields in setter");
            throw new IllegalArgumentException();
        } else {
            this.publisher = publisher;
        }
    }

    public String getEditor() {
        return editor;
    }

    public void setEditor(String editor) {
        if (editor.equals("")) {
            System.out.println("fill in all fields in setter");
            throw new IllegalArgumentException();
        } else {
            this.editor = editor;
        }
    }

    @Override
    public String toString() {
        return super.toString() + "Book{" +
                "author='" + author + '\'' +
                ", publisher='" + publisher + '\'' +
                ", editor='" + editor + '\'' +
                '}';
    }
}
