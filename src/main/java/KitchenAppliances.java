public class KitchenAppliances extends Product {

    private String model;
    private int size;
    private int litres;

    public KitchenAppliances() {
    }

    public KitchenAppliances(String barcode, String description, String manufacture) {
        super(barcode, description, manufacture);
    }

    public KitchenAppliances(String barcode, String description, String manufacture, String model, int size, int litres) {
        super(barcode, description, manufacture);
        setLitres(litres);
        setModel(model);
        setSize(size);
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        if (model.equals("")) {
            System.out.println("fill in all fields in setter");
            throw new IllegalArgumentException();
        } else {
            this.model = model;
        }
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        if (size == 0) {
            System.out.println("fill in all fields in setter");
            throw new IllegalArgumentException();
        } else {
            this.size = size;
        }
    }

    public int getLitres() {
        return litres;
    }

    public void setLitres(int litres) {
        if (litres == 0) {
            System.out.println("fill in all fields in setter");
            throw new IllegalArgumentException();
        } else {
            this.litres = litres;
        }
    }

    @Override
    public String toString() {
        return super.toString() + "KitchenAppliances{" +
                "model='" + model + '\'' +
                ", size=" + size +
                ", litres=" + litres +
                '}';
    }
}
