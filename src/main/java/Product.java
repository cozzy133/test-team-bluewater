public class Product {

    private String barcode;
    private String description;
    private String manufacture;

    public Product() {
    }

    public Product(String barcode, String description, String manufacture) {
        setBarcode(barcode);
        setDescription(description);
        setManufacture(manufacture);
    }

    public String getManufacture() {
        return manufacture;
    }

    public void setManufacture(String manufacture) {
        if (manufacture.equals("")) {
            System.out.println("fill in all fields in setter");
            throw new IllegalArgumentException();
        } else {
            this.manufacture = manufacture;
        }
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        if (barcode.equals("")) {
            System.out.println("fill in all fields in setter");
            throw new IllegalArgumentException();
        } else {
            this.barcode = barcode;
        }
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        if (description.equals("")) {
            System.out.println("fill in all fields in setter");
            throw new IllegalArgumentException();
        } else {
            this.description = description;
        }
    }

    @Override
    public String toString() {
        return "Product{" +
                "barcode='" + barcode + '\'' +
                ", description='" + description + '\'' +
                ", manufacture='" + manufacture + '\'' +
                '}';
    }
}
