/*
 * Author : Padraig O Cosgora
 * Email : G00311302@gmit.ie
 *
 * The HomeDeliveryOrderApplication class verifies the number of testing kits available and allocates them
 * to a HomeDeliveryOrderType object.
 *
 * It does this with a synchronized methods of consumers order projects (the get method) and the producer producing
 * products (the put method). It ensures only one thread/object accesses or provides testing kits at one time.
 */

import java.util.ArrayList;

public class DeliveryOrderApplication {
    ArrayList<Product> delivery = new ArrayList<>();
    private int value;
    private boolean haveValue = false;

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public boolean isHaveValue() {
        return haveValue;
    }

    public void setHaveValue(boolean haveValue) {
        this.haveValue = haveValue;
    }

    /**
     * This method is used to synchronize orders from the server between different purchase points (threads)
     *
     * @param orderAmount The amount of test kits ordered
     * @param orderID     the unique order ID number
     */
    public synchronized int get(int orderAmount, String orderID) {
        ArrayList<Product> toRemove = new ArrayList<>();
        if (!haveValue) {
            try {
                System.out.println(Thread.currentThread().getName() + " waiting on get() ... ");
                wait();  // release lock until notify from another thread
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        haveValue = false;
        if (orderAmount > 6) {
            System.out.println("Max order is six testing kits per delivery, order set to 6.");
            orderAmount = 6;
        }
        //System.out.println("Thread in get function : " + Thread.currentThread().getName());
        //takeNap(15); // simulate a long computation
        notifyAll();  // notify any threads waiting on this one to wake up
        if (value < 100 && value != 0)
            System.out.println("\n######### Warning #########\nStock is LOW!\nRemaining stock : " + value + "\n");
        if (value == 0) {
            System.out.println("\nPlaced Order ID : " + orderID + " total of : " + orderAmount +
                    " not currently in stock. Order cancelled.\n\n######### Zero stock remaining #########\n" +
                    "Waiting for new stock before new orders can be placed.");
            return value;
        } else if ((value - orderAmount) >= 0) {

            value -= orderAmount;
            System.out.println("Order Confirmed & Dispatched! ID: " + orderID + ". Ordered : " + orderAmount +
                    ". Dispatched : " + orderAmount + " unit/s. ");
            //System.out.println("Remaining : " + value);
            haveValue = true;
            notifyAll();
            return orderAmount;
        } else {
            int temp = value;
            value -= temp;
            System.out.println("Placed Order ID : " + orderID + " total of : " + orderAmount +
                    " not currently in stock. " + temp + " dispatched instead.");
            System.out.println("Order Confirmed & Dispatched! ID: " + orderID + ". Ordered : " + orderAmount +
                    ". Dispatched : " + temp + " unit/s. ");
            //System.out.println("Remaining : " + value);
            haveValue = true;
            notifyAll();
            return temp;
        }
    }

    /**
     * This method is used to synchronize production of test kits
     * from the server between different producers (threads)
     *
     * @param i The amount of test kits produced
     */
    public synchronized void put(ArrayList<Product> i) {
        if (haveValue) {
            try {
                System.out.println("put(" + i + ") waiting ... ");
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        delivery.addAll(i);
        value = delivery.size();
        //value = i;
        System.out.println(i + " testing Kits are now available to order.\n");
        //takeNap(200); // simulate a long computation
        haveValue = true;
        notifyAll();
    }

    /**
     * This method is used to represent huge computations
     *
     * @param msecs The amount of msecs to sleep for
     */
    public void takeNap(int msecs) {
        try {
            Thread.sleep(msecs);
        } catch (InterruptedException e) {
            javax.swing.JOptionPane.showMessageDialog(null, "Sleep interrupted!");
        }
    }
}