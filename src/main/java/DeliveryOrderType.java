/*
 * Author : Padraig O Cosgora
 * Email : G00311302@gmit.ie
 *
 * The HomeDeliveryOrderType class extends Thread and creates an instance of a purchase method for testing kits.
 * These instances can purchase testing kits from the HomeDeliveryOrderApplication object.
 *
 */

import java.util.Random;

public class DeliveryOrderType extends Thread {
    private DeliveryOrderApplication orderBuf;
    private String orderTypeID;
    private double cost = 0, totalCost = 0;
    private int minOrderTime, maxOrderTime, totalOrderTime = 0, orderCount = 0, totalOrdered = 0, totalReceived = 0;

    /**
     * This is the only constructor for the HomeDeliveryOrderType class
     *
     * @param c            The HomeDeliveryOrderApplication object
     * @param id           unique ID of instance
     * @param minOrderTime minimum amount of time to place an order
     * @param maxOrderTime maximum amount of time to place an order
     * @param cost         the cost of each order placed
     */
    public DeliveryOrderType(DeliveryOrderApplication c, String id, int minOrderTime, int maxOrderTime,
                             double cost) {
        setOrderBuf(c);
        setMinOrderTime(minOrderTime);
        setMaxOrderTime(maxOrderTime);
        setCost(cost);
        setOrderTypeID(id);
    }



    /**
     * This method returns a random number in a defined range
     *
     * @param min The minimum order time
     * @param max The maximum order time
     */
    public int getRandomNumberInRange(int min, int max) {
        if (min >= max) {
            throw new IllegalArgumentException("max must be greater than min");
        }
        Random r = new Random();
        return r.nextInt((max - min) + 1) + min;
    }

    /**
     * This method runs in a separate thread, and places orders for a random amount of test kits (from 1 to 6)
     * It keeps count of the ordered items and received items
     */
    public void run() {
        while (true) {
            int value;
            int orderAmount = getRandomNumberInRange(1, 6);
            totalOrdered += orderAmount;
            value = orderBuf.get(orderAmount, orderTypeID + orderCount);
            //System.out.println("Consumer : " + orderTypeID + orderID + " ordered : " + orderAmount + " got: "
            // + value);
            totalReceived += value;
            if (value == 0) {
                System.out.println("Order cancelled! ID : " + orderTypeID + orderCount + ". Order : " + orderAmount +
                        " unit/s.\n");
            } else {
                System.out.println("Order Arrived! ID : " + orderTypeID + orderCount + ". Ordered : " + orderAmount +
                        ". Received: " + value + " unit/s.");
                //System.out.println(" Cost per order: €" + cost + " Total cost : €" + result);
                totalCost += cost;
                orderCount++;
            }
            try {
                int orderTime = getRandomNumberInRange(minOrderTime, maxOrderTime);
                totalOrderTime += orderTime; // includes time of any cancelled orders as they still had to be processed
                sleep((int) (0.015 * orderTime)); // every second = 0.015 s (15 ms)
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * This method connects the HomeDeliveryOrderApplication object to this object to offer synchronization
     *
     * @param orderBuf The HomeDeliveryOrderApplication object
     */
    public void setOrderBuf(DeliveryOrderApplication orderBuf) {
        this.orderBuf = orderBuf;
    }

    public int getMinOrderTime() {
        return minOrderTime;
    }

    /**
     * This method sets the minimum order time
     *
     * @param minOrderTime The minimum order time
     */
    public void setMinOrderTime(int minOrderTime) {
        if (minOrderTime < 0) {
            throw new IllegalArgumentException("minimum order time must be greater than 0");
        }
        this.minOrderTime = minOrderTime;
    }

    /**
     * This method sets the maximum order time
     *
     * @param maxOrderTime The maximum order time
     */
    public void setMaxOrderTime(int maxOrderTime) {
        if (maxOrderTime < getMinOrderTime()) {
            throw new IllegalArgumentException("maximum order time must be greater than minimum order time");
        }
        this.maxOrderTime = maxOrderTime;
    }

    public int getMaxOrderTime() {
        return  maxOrderTime;
    }


    public void setTotalCost(double cost) {
        if (totalCost < 0) {
            throw new IllegalArgumentException("total cost cannot be less than 0");
        }
        this.totalCost = totalCost;
    }

    /**
     * This method gets the total cost of all orders
     *
     * @return double total cost of all orders
     */
    public double getTotalCost() {
        String result = String.format("%.2f", totalCost);
        return Double.parseDouble(result);
    }

    /**
     * This method sets the order type ID
     *
     * @param orderTypeID The order type ID
     */
    public void setOrderTypeID(String orderTypeID) {
        this.orderTypeID = orderTypeID;
    }

    //setter for ID
    public String getOrderTypeID() {
         return orderTypeID;
    }

    /**
     * This method sets the cost of every delivery
     *
     * @param cost The cost of each delivery
     */
    public void setCost(double cost) {
        if (cost < 0) {
            throw new IllegalArgumentException("cost cannot be less than 0");
        }
        this.cost = cost;
    }

    //setter for cosrt
    public double getCost() {
        return cost;
    }
    /**
     * This method returns the order count number
     *
     * @return int total order count number
     */
    public int getOrderCount() {
        return orderCount;
    }
    public void setOrderCount(int i) {
        if (i < 0) {
            throw new IllegalArgumentException("cost cannot be less than 0");
        }
        this.orderCount = orderCount;
    }


    /**
     * This method returns the total order count
     *
     * @return int total order count
     */
    public int getTotalOrdered() {
        return totalOrdered;
    }

    public void setTotalOrdered(int i) {
        if (i < 0) {
            throw new IllegalArgumentException("cost cannot be less than 0");
        }
        this.totalOrdered = totalOrdered;
    }


    /**
     * This method returns the total orders received
     *
     * @return int total orders received
     */
    public int getTotalReceived() {
        return totalReceived;
    }
    public void setTotalReceived(int i) {
        if (i < 0) {
            throw new IllegalArgumentException("cost cannot be less than 0");
        }
        this.totalReceived = totalReceived;
    }

    @Override
    public String toString() {
        return "\nTotal Orders : " + orderCount +
                "\nTotal Time : " + totalOrderTime +
                " seconds\nTotal Cost : €" + getTotalCost();
    }


}