/*
 * Author : Padraig O Cosgora
 * Email : G00311302@gmit.ie
 *
 * The HDO class run the simulation of the home delivery system.
 */


import java.util.ArrayList;

public class DeliverySystemTest {

    public static void main(String[] args) throws InterruptedException {
        // Create the shared storage
//        DeliveryGui gui = new DeliveryGui();
//        double temp2;
//        boolean running = true;
//        while (running){
//            double temp1 = gui.getMin2();
//            if(temp1 >=1){
//                break;
//            }
//
//            System.out.println(temp1);
//        }
// Get class path by using getProperty static method of System class
        ProductDelivery delivery = new ProductDelivery();
        ArrayList<Product> deliver = delivery.packTruck(500);


        DeliveryOrderApplication orderBuf = new DeliveryOrderApplication();
        // Create the producer & consumer threads

        Production prod = new Production(orderBuf, deliver);
        //Production prod = new Production(orderBuf, 70000);
//        DeliveryOrderType kioskOrder = new DeliveryOrderType(orderBuf, "kioskOrder", (int) gui.getMin2(),
//                (int) gui.getMax2(), gui.getCost2());
//        DeliveryOrderType phoneOrder = new DeliveryOrderType(orderBuf, "phoneOrder",(int) gui.getMin3(),
//                (int) gui.getMax3()  , gui.getCost3());
//        DeliveryOrderType appOrder = new DeliveryOrderType(orderBuf, "appOrder", (int) gui.getMin4(),
//                (int) gui.getMax4(), gui.getCost4());
//        DeliveryOrderType onlineOrder = new DeliveryOrderType(orderBuf, "onlineOrder", (int) gui.getMin5(),
//                (int) gui.getMax5(), gui.getCost5());
        DeliveryOrderType kioskOrder = new DeliveryOrderType(orderBuf, "kioskOrder", 120,
                300, 1.50);
        DeliveryOrderType phoneOrder = new DeliveryOrderType(orderBuf, "phoneOrder", 60,
                120, 0.50);
        DeliveryOrderType appOrder = new DeliveryOrderType(orderBuf, "appOrder", 20,
                40, 0.10);
        DeliveryOrderType onlineOrder = new DeliveryOrderType(orderBuf, "onlineOrder", 40,
                60, 1.00);


        prod.start();  // Start the threads
        kioskOrder.start();
        phoneOrder.start();
        appOrder.start();
        onlineOrder.start();

        Thread.sleep(1000); // main thread sleeps for 1 second as simulation runs

        System.out.println("\nKiosk Orders : " + kioskOrder.toString());
        System.out.println("\nPhone Orders : " + phoneOrder.toString());
        System.out.println("\nApp Orders : " + appOrder.toString());
        System.out.println("\nOnline Orders : " + onlineOrder.toString());

        System.out.println("\nTotal orders : " + (kioskOrder.getOrderCount() + phoneOrder.getOrderCount() +
                appOrder.getOrderCount() + onlineOrder.getOrderCount()));
        System.out.println("\nTotal items ordered : " + (kioskOrder.getTotalOrdered() + phoneOrder.getTotalOrdered() +
                appOrder.getTotalOrdered() + onlineOrder.getTotalOrdered()) +
                " (incl. all orders made before stock ran out)");
        System.out.println("\nTotal items dispatched/delivered : " + (kioskOrder.getTotalReceived() +
                phoneOrder.getTotalReceived() + appOrder.getTotalReceived() + onlineOrder.getTotalReceived()));


        System.exit(0);

    }

}