import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestSoftware {

    /************SETTING UP OBJECT INSTANCE*************/

    private Software software;

    /************SETTING UP PRE TEST*************/
    @BeforeAll
    @DisplayName("Starting tests")
    public static void print(){
        System.out.println("Starting tests");
    }

    @BeforeEach
    @DisplayName("New instance of Production.Software")
    void init(TestInfo testInfo, TestReporter testReporter)
    {
        software = new Software();
        System.out.println("timestamp = " + testInfo.getDisplayName());
    }

    /************TESTING GETTERS AND SETTERS*************/
    @Test
    @DisplayName("Testing Getters and Setters of software")
    void testGettersAndSetters(TestInfo testInfo)
    {
        software.setNumberOfPeople(8);
        assertEquals(software.getNumberOfPeople(),8);
        software.setBarcode("productCode");
        assertEquals(software.getBarcode(),"productCode");
        software.setVersion("23.6");
        assertEquals(software.getVersion(),"23.6");
        software.setDescription("test");
        assertEquals(software.getDescription(), "test");
        software.setManufacture("mcafee");
        assertEquals(software.getManufacture(),"mcafee");
        Assertions.assertThrows(IllegalArgumentException.class, () -> software.setVersion(""));
        Assertions.assertThrows(IllegalArgumentException.class, () -> software.setNumberOfPeople(0));
        String test = software.toString();
        assertEquals(test,software.toString());
    }

    @Test
    @DisplayName("Testing Getters and Setters of Student")
    void testToString(TestInfo testInfo)
    {
        software = new Software("test", "test", "test");
        assertEquals(software.getBarcode(), "test");
        assertEquals(software.getDescription(), "test");
        assertEquals(software.getManufacture(), "test");
    }

    @Test
    @DisplayName("Invalid Version setter test")
    void testInvalidVersion(TestInfo testInfo) {
        Assertions.assertThrows(IllegalArgumentException.class, () -> software.setVersion(""));
    }

    @Test
    @DisplayName("Invalid NumberOfPeople setter test")
    void testInvalidNumberOfPeople(TestInfo testInfo) {
        Assertions.assertThrows(IllegalArgumentException.class, () -> software.setNumberOfPeople(0));
    }
}
