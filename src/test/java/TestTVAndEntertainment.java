import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.*;

public class TestTVAndEntertainment {

    /************SETTING UP OBJECT INSTANCE*************/

    private TvAndEntertainment tvAndEntertainment;

    /************SETTING UP PRE TEST*************/
    @BeforeAll
    @DisplayName("Starting tests")
    public static void print(){
        System.out.println("Starting tests");
    }

    @BeforeEach
    @DisplayName("New instance of Production.Software")
    void init(TestInfo testInfo, TestReporter testReporter)
    {
        tvAndEntertainment = new TvAndEntertainment();
        System.out.println("timestamp = " + testInfo.getDisplayName());
    }

    /************TESTING GETTERS AND SETTERS*************/
    @Test
    @DisplayName("Testing Getters and Setters of tvAndEntertainment")
    void testGettersAndSetters(TestInfo testInfo)
    {
        tvAndEntertainment.setScreenSize("65");
        assertEquals(tvAndEntertainment.getScreenSize(),"65");
        tvAndEntertainment.setBarcode("productCode");
        assertEquals(tvAndEntertainment.getBarcode(),"productCode");
        tvAndEntertainment.setQuality("4K");
        assertEquals(tvAndEntertainment.getQuality(),"4K");
        tvAndEntertainment.setDescription("goodTV");
        assertEquals(tvAndEntertainment.getDescription(),"goodTV");
        tvAndEntertainment.setManufacture("samsung");
        assertEquals(tvAndEntertainment.getManufacture(),"samsung");
        tvAndEntertainment.setModel("oled");
        assertEquals(tvAndEntertainment.getModel(), "oled");
        tvAndEntertainment.setServices(true);
        assertTrue(tvAndEntertainment.isServices());
    }

    @Test
    @DisplayName("Invalid Model setter test")
    void testInvalidModel(TestInfo testInfo) {
        Assertions.assertThrows(IllegalArgumentException.class, () -> tvAndEntertainment.setModel(""));
        Assertions.assertThrows(IllegalArgumentException.class, () -> tvAndEntertainment.setQuality(""));
        Assertions.assertThrows(IllegalArgumentException.class, () -> tvAndEntertainment.setScreenSize(""));
        Assertions.assertThrows(IllegalArgumentException.class, () -> tvAndEntertainment.setServices(false));
        Assertions.assertThrows(IllegalArgumentException.class, () -> tvAndEntertainment.setModel(""));
        tvAndEntertainment = new TvAndEntertainment("test","test","test");
        assertNull(tvAndEntertainment.getModel());
        String test = tvAndEntertainment.toString();
        assertEquals(test,tvAndEntertainment.toString());
    }

    @Test
    @DisplayName("Invalid ScreenSize setter test")
    void testInvalidScreenSize(TestInfo testInfo) {
        Assertions.assertThrows(IllegalArgumentException.class, () -> tvAndEntertainment.setScreenSize(""));
    }

    @Test
    @DisplayName("Invalid Quality setter test")
    void testInvalidQuality(TestInfo testInfo) {
        Assertions.assertThrows(IllegalArgumentException.class, () -> tvAndEntertainment.setQuality(""));
    }

    @Test
    @DisplayName("Invalid Services setter test")
    void testInvalidServices(TestInfo testInfo) {
        Assertions.assertThrows(IllegalArgumentException.class, () -> tvAndEntertainment.setServices(false));
    }
}
