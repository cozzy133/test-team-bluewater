import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

public class TestComputing {
    /************SETTING UP OBJECT INSTANCE*************/

    private Computing computing;
    private Computing comp1;


    /************SETTING UP PRE TEST*************/
    @BeforeAll
    @DisplayName("Starting tests")
    public static void print(){
        System.out.println("Starting tests");
    }

    @BeforeEach
    @DisplayName("New instance of computing")
    void init(TestInfo testInfo, TestReporter testReporter)
    {
        computing = new Computing();
        comp1 = new Computing("121212", "Fancy new Computer", "Nvidia",
                "Intel i9 9700k", "8 gb 2666 mhz", "25 inch");
        System.out.println("timestamp = " + testInfo.getDisplayName());
    }

    /************TESTING GETTERS AND SETTERS*************/
    @Test
    @DisplayName("Testing Getters and Setters of computing")
    void testGettersAndSetters(TestInfo testInfo)
    {
        computing.setScreenSize("15.6");
        assertEquals(computing.getScreenSize(),"15.6");
        computing.setBarcode("productCode");
        assertEquals(computing.getBarcode(),"productCode");
        computing.setRam("8GB");
        assertEquals(computing.getRam(),"8GB");
        computing.setCpu("intel I5");
        assertEquals(computing.getCpu(),"intel I5");
        computing.setManufacture("HP");
        assertEquals(computing.getManufacture(),"HP");
        computing.setDescription("top of the range Dell Xps15");
        assertEquals(computing.getDescription(), "top of the range Dell Xps15");
    }

    @Test
    @DisplayName("Invalid Cpu setter test")
    void testInvalidCpu(TestInfo testInfo) {
        Assertions.assertThrows(IllegalArgumentException.class, () -> computing.setCpu(""));
    }

    @Test
    @DisplayName("Invalid Ram setter test")
    void testInvalidRam(TestInfo testInfo) {
        Assertions.assertThrows(IllegalArgumentException.class, () -> computing.setRam(""));
    }

    @Test
    @DisplayName("Invalid ScreenSize setter test")
    void testInvalidScreenSize(TestInfo testInfo) {
        Assertions.assertThrows(IllegalArgumentException.class, () -> computing.setScreenSize(""));
    }

    @Test
    void getCpu() {
        assertEquals("Intel i9 9700k", comp1.getCpu());
    }

    @Test
    void setCpu() {
        comp1.setCpu("Intel i7 9700k");
        assertEquals("Intel i7 9700k", comp1.getCpu());
        Assertions.assertThrows(IllegalArgumentException.class, () -> comp1.setCpu(""));

    }

    @Test
    void setDefaultCon() {
        comp1 = new Computing();
        assertNull(comp1.getCpu());
    }
    @Test
    void setSuperCon() {
        comp1 = new Computing("test", "test", "test");
        assertEquals("test", comp1.getBarcode());
    }

    @Test
    void getRam() {
        assertEquals("8 gb 2666 mhz", comp1.getRam());
    }

    @Test
    void setRam() {
        comp1.setRam("8gb");
        assertEquals("8gb", comp1.getRam());
        Assertions.assertThrows(IllegalArgumentException.class, () -> comp1.setRam(""));
    }

    @Test
    void getScreenSize() {
        assertEquals("25 inch", comp1.getScreenSize());
    }

    @Test
    void setScreenSize() {
        comp1.setScreenSize("24 inch");
        assertEquals("24 inch", comp1.getScreenSize());
        Assertions.assertThrows(IllegalArgumentException.class, () -> comp1.setScreenSize(""));
    }

    @Test
    void testToString() {
        String test = comp1.toString();
        assertEquals(test, comp1.toString());
    }
}
