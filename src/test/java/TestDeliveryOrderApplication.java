/*

Production.Software with Test Submission
Team: BlueWater
Team Members:Paulina Osikoya, Padraig O Cosgora, Stephen Mcintyre, Armen Petrosyan
lecturer:Paul lennon

Brief : this section is where all the test for the code will be carried out for the software with test project

*/

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.*;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.assertThat;
import static org.junit.jupiter.api.Assertions.assertFalse;

import java.util.ArrayList;

public class TestDeliveryOrderApplication {
    private DeliveryOrderApplication myDeliveryOrderApplication;
    private DeliverySystemTest dst;


    /**************************PRE TEST START**************************/
    @BeforeAll
    @DisplayName("Starting the Test")
    public static void print(){
        System.out.println("Starting the tests.");
    }
    /*****************************************************************/

    /**************************CREATE INSTANCE**************************/
    /**
     * @param  @BeforeEach will create an instance of KioskOrder, phoneOrder, onlineOrder and AppOrder
     */
    @BeforeEach
    @DisplayName("Setting a New instance of the Employee")
    void init(TestInfo testInfo, TestReporter testReporter)
    {
        myDeliveryOrderApplication = new DeliveryOrderApplication();
    }
    /*****************************************************************/


    /************************** TESTS **************************/
    @Test
    @DisplayName("valid Method Test")
    void testOnlineConstructorInvalid(TestInfo testInfo, TestReporter testReporter)
    {
        myDeliveryOrderApplication.takeNap(100);
        myDeliveryOrderApplication.setHaveValue(false);
        assertFalse(myDeliveryOrderApplication.isHaveValue());
        myDeliveryOrderApplication.setValue(10);
        assertEquals(10, myDeliveryOrderApplication.getValue());
//        myDeliveryOrderApplication.put(10);
//        assertEquals( myDeliveryOrderApplication.getValue(),10);
//        myDeliveryOrderApplication.setValue(10);
//        assertEquals( myDeliveryOrderApplication.getValue(),10);
//        myDeliveryOrderApplication.setHaveValue(true);
//        assertEquals( myDeliveryOrderApplication.isHaveValue(),true);

    }

    /************************** TESTS **************************/
    @Test
    @DisplayName("valid Method Test")
    void testOnlineConstructorInvalid1(TestInfo testInfo, TestReporter testReporter)
    {
        ArrayList<Product> test = new ArrayList<Product>();
        ProductDelivery testing = new ProductDelivery();
        testing.packTruck(10);
        assertThat(testing, instanceOf(ProductDelivery.class));
        Book book = new Book();
        assertThat(book, instanceOf(Book.class));
        test.add(book);
        myDeliveryOrderApplication = new DeliveryOrderApplication();
        dst = new DeliverySystemTest();
        myDeliveryOrderApplication.put(test);
        myDeliveryOrderApplication.get(10, "1000");

    }


    /**************************POST TEST DONE**************************/
    @AfterAll
    @DisplayName("Ending the Test")
    public static void done(){
        System.out.println("Completed the tests.");
    }
    /*****************************************************************/

}
