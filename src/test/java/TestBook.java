import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

public class TestBook {
    /************SETTING UP OBJECT INSTANCE*************/

    private Book book;

    /************SETTING UP PRE TEST*************/
    @BeforeAll
    @DisplayName("Starting tests")
    public static void print() {
        System.out.println("Starting tests");
    }

    @BeforeEach
    @DisplayName("New instance of book")
    void init(TestInfo testInfo, TestReporter testReporter) {
        book = new Book();
        System.out.println("timestamp = " + testInfo.getDisplayName());
    }

    /************TESTING GETTERS AND SETTERS*************/
    @Test
    @DisplayName("Testing Getters and Setters of book")
    void testGettersAndSetters(TestInfo testInfo) {
        book.setPublisher("HarperCollins");
        assertEquals(book.getPublisher(), "HarperCollins");
        book.setEditor("Darren Shan");
        assertEquals(book.getEditor(), "Darren Shan");
        book.setBarcode("ProductCode");
        assertEquals(book.getBarcode(), "ProductCode");
        book.setAuthor("J.k rowling");
        assertEquals(book.getAuthor(), "J.k rowling");
        book.setDescription("The Saga of Darren Shan young adult fiction series");
        assertEquals(book.getDescription(), "The Saga of Darren Shan young adult fiction series");
        Assertions.assertThrows(IllegalArgumentException.class, () -> book.setEditor(""));
        Assertions.assertThrows(IllegalArgumentException.class, () -> book.setAuthor(""));
        Assertions.assertThrows(IllegalArgumentException.class, () -> book.setPublisher(""));
        book = new Book();
        assertNull(book.getAuthor());
        String test = book.toString();
        assertEquals(test, book.toString());
        book = new Book("test", "test", "test");
        assertNull(book.getAuthor());
    }

    @Test
    @DisplayName("Testing Getters and Setters of Student")
    void testToString(TestInfo testInfo)
    {
    }

    @Test
    @DisplayName("Invalid Author setter test")
    void testInvalidAuthor(TestInfo testInfo) {
        Assertions.assertThrows(IllegalArgumentException.class, () -> book.setAuthor(""));
    }

    @Test
    @DisplayName("Invalid Editor setter test")
    void testInvalidEditor(TestInfo testInfo) {
        Assertions.assertThrows(IllegalArgumentException.class, () -> book.setEditor(""));
    }

    @Test
    @DisplayName("Invalid Publisher setter test")
    void testInvalidPublisher(TestInfo testInfo) {
        Assertions.assertThrows(IllegalArgumentException.class, () -> book.setPublisher(""));
    }


}