import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestProduct {
    /************SETTING UP OBJECT INSTANCE*************/

    private Product product;

    /************SETTING UP PRE TEST*************/
    @BeforeAll
    @DisplayName("Starting tests")
    public static void print(){
        System.out.println("Starting tests");
    }

    @BeforeEach
    @DisplayName("New instance of product")
    void init(TestInfo testInfo, TestReporter testReporter)
    {
        product = new Product();
        System.out.println("timestamp = " + testInfo.getDisplayName());
    }

    /************TESTING GETTERS AND SETTERS*************/

    @Test
    @DisplayName("Testing Getters and Setters of product")
    void testGettersAndSetters(TestInfo testInfo)
    {
        product.setBarcode("productCode");
        assertEquals(product.getBarcode(),"productCode");
        product.setManufacture("samsung");
        assertEquals(product.getManufacture(),"samsung");
        product.setDescription("UnrealProduct");
        assertEquals(product.getDescription(), "UnrealProduct");
    }

    @Test
    @DisplayName("Valid Constructor test")
    void testProductConstructorValid(TestInfo testInfo) {
        //Product p1 = new Product("productCode", "UnrealProduct", "samsung");
        product.setDescription("UnrealProduct");
        product.setManufacture("samsung");
        product.setBarcode("productCode");
        assertEquals("productCode", product.getBarcode());
        assertEquals("UnrealProduct", product.getDescription());
        assertEquals("samsung", product.getManufacture());
    }

    @Test
    @DisplayName("Invalid Constructor Test (invalid barcode)")
    void testConstructorInvalidBarcode(TestInfo testInfo) {
        Assertions.assertThrows(IllegalArgumentException.class, () -> product = new Product("", "UnrealProduct", "samsung"));
        Assertions.assertThrows(IllegalArgumentException.class, () -> product.setManufacture(""));
        Assertions.assertThrows(IllegalArgumentException.class, () -> product.setBarcode(""));
        Assertions.assertThrows(IllegalArgumentException.class, () -> product.setDescription(""));

    }

    @Test
    @DisplayName("Invalid Constructor Test (invalid Description)")
    void testConstructorInvalidDescription(TestInfo testInfo) {
        Assertions.assertThrows(IllegalArgumentException.class, () -> new Product("productCode", "", "samsung"));
    }
    @Test
    @DisplayName("Invalid Constructor Test (invalid Manufacture)")
    void testConstructorInvalidManufacture(TestInfo testInfo) {
        Assertions.assertThrows(IllegalArgumentException.class, () -> new Product("productCode", "UnrealProduct", ""));
    }

    @Test
    @DisplayName("Invalid Description setter test")
    void testInvalidDescription(TestInfo testInfo) {
        Assertions.assertThrows(IllegalArgumentException.class, () -> product.setDescription(""));
    }

    @Test
    @DisplayName("Invalid Manufacture setter test")
    void testInvalidManufacture(TestInfo testInfo) {
        Assertions.assertThrows(IllegalArgumentException.class, () -> product.setManufacture(""));
    }

    @Test
    @DisplayName("Invalid Barcode setter test")
    void testInvalidBarcode(TestInfo testInfo) {
        Assertions.assertThrows(IllegalArgumentException.class, () -> product.setBarcode(""));
    }

    /************TESTING COMPLETE*************/

    @AfterAll
    @DisplayName("Testing complete")
    public static void done() {
        System.out.println("Testing Complete");
    }
}
