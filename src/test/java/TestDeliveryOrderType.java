/*

Production.Software with Test Submission
Team: BlueWater
Team Members:Paulina Osikoya, Padraig O Cosgora, Stephen Mcintyre, Armen Petrosyan
lecturer:Paul lennon

Brief : this section is where all the test for the code will be carried out for the software with test project

*/

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.*;

public class TestDeliveryOrderType {
    private DeliveryOrderType kioskOrder,phoneOrder,appOrder,onlineOrder;
    private DeliveryOrderApplication orderBuf;

    /**************************PRE TEST START**************************/
    @BeforeAll
    @DisplayName("Starting the Test")
    public static void print(){
        System.out.println("Starting the tests.");
    }
    /*****************************************************************/

    /**************************CREATE INSTANCE**************************/
    /**
     * @param  @BeforeEach will create an instance of KioskOrder, phoneOrder, onlineOrder and AppOrder
     */
    @BeforeEach
    @DisplayName("Setting a New instance of the Employee")
    void init(TestInfo testInfo, TestReporter testReporter)
    {
        orderBuf = new DeliveryOrderApplication();
        kioskOrder = new DeliveryOrderType(orderBuf, "kioskOrder", 120,
                300, 1.50);
        kioskOrder.setTotalCost(1000);
        //assertEquals(1000, kioskOrder.getTotalCost());
        //Assertions.assertThrows(IllegalArgumentException.class, () -> kioskOrder.setTotalCost(-100));
        Assertions.assertThrows(IllegalArgumentException.class, () -> kioskOrder.setMaxOrderTime(-100));
        Assertions.assertThrows(IllegalArgumentException.class, () -> kioskOrder.setMinOrderTime(-100));
        Assertions.assertThrows(IllegalArgumentException.class, () -> kioskOrder.setCost(-100));
        Assertions.assertThrows(IllegalArgumentException.class, () -> kioskOrder.setOrderCount(-100));
        Assertions.assertThrows(IllegalArgumentException.class, () -> kioskOrder.setTotalOrdered(-100));
        Assertions.assertThrows(IllegalArgumentException.class, () -> kioskOrder.setTotalReceived(-100));
        int test = kioskOrder.getRandomNumberInRange(1, 2);
        Assertions.assertThrows(IllegalArgumentException.class, () -> kioskOrder.getRandomNumberInRange(2, 2));
        //assertEquals( 2, test);
    }
    /*****************************************************************/


    /************************** TESTS **************************/
    @Test
    @DisplayName("valid Method Test")
    void testOnlineConstructorInvalid(TestInfo testInfo, TestReporter testReporter)
    {

        //GET Min Order
        kioskOrder.setMinOrderTime(120);
        assertEquals(kioskOrder.getMinOrderTime(),120);

        //GET MaxOrder
        kioskOrder.setMaxOrderTime(300);
        assertEquals(kioskOrder.getMaxOrderTime(),300);

        //GET Cost
        kioskOrder.setCost(1.50);
        assertEquals( kioskOrder.getCost(),1.50);


    }
    /************************** invalid Run Method**************************/

    @Test
    @DisplayName("invalid Constructor online")
    void testRunMethodInvalid(TestInfo testInfo, TestReporter testReporter)
    {

//        Assertions.assertThrows(IllegalArgumentException.class, ()->{
//            new Production(orderBuf, -2);
//        });
    }
    /*************************Get Total Cost**************************/

    @Test
    @DisplayName("Valid Total Cost ")
    void testTotalCostValid(TestInfo testInfo, TestReporter testReporter)
    {

        kioskOrder.setTotalCost(0.0);
        assertEquals(kioskOrder.getTotalCost(),0.0);

    }

    /*************************Order count  Total order recieved and ordered methods**************************/

    @Test
    @DisplayName("Valid Total order")
    void testOrderCountValid(TestInfo testInfo, TestReporter testReporter)
    {

        kioskOrder.setOrderCount(0);
        assertEquals(kioskOrder.getOrderCount(),0);


        kioskOrder.setTotalOrdered(0);
        assertEquals(kioskOrder.getTotalOrdered(),0);


        kioskOrder.setTotalReceived(0);
        assertEquals(kioskOrder.getTotalReceived(),0);

    }

    @Test
    @DisplayName("inValid Total order")
    void testOrderCountInValid(TestInfo testInfo, TestReporter testReporter)
    {
        Assertions.assertThrows(IllegalArgumentException.class, ()->{
            kioskOrder.setOrderCount(-1);
        });

        Assertions.assertThrows(IllegalArgumentException.class, ()->{
            kioskOrder.setTotalReceived(-1);
        });

        Assertions.assertThrows(IllegalArgumentException.class, ()->{
            kioskOrder.setTotalOrdered(-1);
        });
    }




    /**************************POST TEST DONE**************************/
    @AfterAll
    @DisplayName("Ending the Test")
    public static void done(){
        System.out.println("Completed the tests.");
    }
    /*****************************************************************/

}
