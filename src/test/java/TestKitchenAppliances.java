import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

public class TestKitchenAppliances {
    /************SETTING UP OBJECT INSTANCE*************/

    private KitchenAppliances kitchenAppliances;
    
    /************SETTING UP PRE TEST*************/
    @BeforeAll
    @DisplayName("Starting tests")
    public static void print() {
        System.out.println("Starting tests");
    }

    @BeforeEach
    @DisplayName("New instance of kitchenAppliances")
    void init(TestInfo testInfo, TestReporter testReporter) {
        kitchenAppliances = new KitchenAppliances();
        System.out.println("timestamp = " + testInfo.getDisplayName());
    }

    /************TESTING GETTERS AND SETTERS*************/
    @Test
    @DisplayName("Testing Getters and Setters of kitchenAppliances")
    void testGettersAndSetters(TestInfo testInfo) {
        kitchenAppliances.setLitres(7);
        assertEquals(kitchenAppliances.getLitres(), 7);
        kitchenAppliances.setManufacture("samsung");
        assertEquals(kitchenAppliances.getManufacture(), "samsung");
        kitchenAppliances.setBarcode("ProductCode");
        assertEquals(kitchenAppliances.getBarcode(), "ProductCode");
        kitchenAppliances.setSize(10);
        assertEquals(kitchenAppliances.getSize(), 10);
        kitchenAppliances.setModel("american");
        assertEquals(kitchenAppliances.getModel(), "american");
        kitchenAppliances.setDescription("top of the range fridge");
        assertEquals(kitchenAppliances.getDescription(), "top of the range fridge");
    }

    @Test
    @DisplayName("Testing Getters and Setters of Student")
    void testToString(TestInfo testInfo) {
        Assertions.assertThrows(IllegalArgumentException.class, () -> kitchenAppliances.setLitres(0));
        Assertions.assertThrows(IllegalArgumentException.class, () -> kitchenAppliances.setModel(""));
        Assertions.assertThrows(IllegalArgumentException.class, () -> kitchenAppliances.setSize(0));
        kitchenAppliances = new KitchenAppliances("test","test","test");
        assertNull(kitchenAppliances.getModel());
        String test = kitchenAppliances.toString();
        assertEquals(test,kitchenAppliances.toString());
    }

    @Test
    @DisplayName("Invalid Model setter test")
    void testInvalidModel(TestInfo testInfo) {
        Assertions.assertThrows(IllegalArgumentException.class, () -> kitchenAppliances.setModel(""));
    }

    @Test
    @DisplayName("Invalid Size setter test")
    void testInvalidSize(TestInfo testInfo) {
        Assertions.assertThrows(IllegalArgumentException.class, () -> kitchenAppliances.setSize(0));
    }

    @Test
    @DisplayName("Invalid Litres setter test")
    void testInvalidLitres(TestInfo testInfo) {
        Assertions.assertThrows(IllegalArgumentException.class, () -> kitchenAppliances.setLitres(0));
    }
}
